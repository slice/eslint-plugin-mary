# eslint-plugin-mary

![mary approved rules](https://wolf-odonnell.is-for.me/i/j96jbd24.png)

## install

```bash
# globally:
$ npm i -g git+https://gitlab.com/slcx/eslint-plugin-mary.git

# or in your project:
$ npm i -D git+https://gitlab.com/slcx/eslint-plugin-mary.git
```

## usage

add to your eslintrc:

```json
{
  "extends": [..., "plugin:mary/recommended"],
  "plugins": [..., "mary"]
}
```
