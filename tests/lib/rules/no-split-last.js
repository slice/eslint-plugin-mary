const rule = require('../../../lib/rules/no-split-last')
const { RuleTester } = require('eslint')

const tester = new RuleTester()

tester.run('no-split-last', rule, {
  invalid: [
    {
      code: `
        var fspath = 'hello.world'
        var chunks = fspath.split('.')
        var last = chunks[chunks.length - 1]
      `,
      errors: [{ message: 'lastIndexOf and substring should be used for enhanced performance' }],
    },
  ],
  valid: [
    {
      code: `
        var fspath = 'hello.world'
        var last = fspath.substring(fspath.lastIndexOf('.') + 1)
      `,
    },
  ],
})
